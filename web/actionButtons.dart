part of darkagesgame;

int buttonHeight = 30;
int buttonWidth = 115;
var listOfButtons = new List(8);

setupButtons()
{
  listOfButtons[0] = createButton("endTurn", "endTurnDepressed", 50, 165, 60, 140, stepTurn, listMenu);
  listOfButtons[1] = createButton("barricade", "barricadeDepressed", 17, 150, buttonHeight, buttonWidth, clickOnBarricade, actionMenu);
  listOfButtons[2] = createButton("orbitalStrike", "orbitalStrikeDepressed",  17, 190, buttonHeight, buttonWidth, clickOnOrbitalStrike, actionMenu);
  listOfButtons[3] = createButton("quarantine", "quarantineDepressed",  17, 230, buttonHeight, buttonWidth, clickOnQuarantine, actionMenu);
  listOfButtons[4] = createButton("houseArrest","houseArrestDepressed", 17, 270, buttonHeight, buttonWidth, clickOnHouseArrest, actionMenu);
  listOfButtons[5] = createButton("uproot", "uprootDepressed", 17, 310, buttonHeight, buttonWidth, clickOnUproot, actionMenu);
  listOfButtons[6] = createButton("vaccinate", "vaccinateDepressed", 17, 350, buttonHeight, buttonWidth, clickOnVaccinate, actionMenu);
  listOfButtons[7] = createButton("cull", "cullDepressed", 17, 390, buttonHeight, buttonWidth, clickOnCull, actionMenu);
}

createButton(bitmapName, bitmapNameDepressed, int xCoord, int yCoord, height, width, listenFunction, parentObject)
{
  var thisButton = new Bitmap(resourceManager.getBitmapData(bitmapName));
  var thisButtonDepressed = new Bitmap(resourceManager.getBitmapData(bitmapNameDepressed));
  thisButton.y = yCoord;
  thisButton.x = xCoord;
  thisButton.height = height;
  thisButton.width = width;
  
  thisButtonDepressed.y = yCoord;
  thisButtonDepressed.x = xCoord;
  thisButtonDepressed.height = height;
  thisButtonDepressed.width = width;

  Sprite thisSprite = new Sprite();
  thisSprite.addChild(thisButton);
  parentObject.addChild(thisSprite);
  thisSprite.onMouseClick.listen(listenFunction);
  
  depressedButton(MouseEvent e)
  {
    thisSprite.addChild(thisButtonDepressed);
  }
  
  regularButton(MouseEvent e)
  {
    thisSprite.addChild(thisButton);
  }
  
  thisSprite.onMouseDown.listen(depressedButton);
  thisSprite.onMouseUp.listen(regularButton);
  thisSprite.onMouseOut.listen(regularButton);
  
  return thisSprite;
}

refreshButtons()
{
  if (colony.selected_city.orbital_strike.ready())
  {
    listOfButtons[2].alpha = 1;
  }
  else
  {
    listOfButtons[2].alpha = .25;
  }
  
  if (colony.selected_city.vaccinate.ready())
  {
    listOfButtons[6].alpha = 1;
  }
  else
  {
    listOfButtons[6].alpha = .25;
  }
  
  if (colony.selected_city.cull.ready())
  {
    listOfButtons[7].alpha = 1;
  }
  else
  {
    listOfButtons[7].alpha = .25;
  }
  
  if (colony.selected_city.uproot.ready())
  {
    listOfButtons[5].alpha = 1;
  }
  else
  {
    listOfButtons[5].alpha = .25;
  }
  
  
  if (!colony.selected_city.barricade.active)
  {
    listOfButtons[1].alpha = 1;
  }
  else
  {
    listOfButtons[1].alpha = .25;
  }
  
  
  if (!colony.selected_city.quarantine.active)
  {
    listOfButtons[3].alpha = 1;
  }
  else
  {
    listOfButtons[3].alpha = .25;
  }

  if (!colony.selected_city.house_arrest.active)
  {
    listOfButtons[4].alpha = 1;
  }
  else
  {
    listOfButtons[4].alpha = .25;
  }
   
}


