part of darkagesgame;

final spread_from_const = 3;
final spread_within_const = 5;
final death_const = 0.85;
final game_turns = 25;

class Integer {
  int val = 0;
}

class Action {
  static List act_list = new List();
  Integer count;
  int max_count = 1;
  var exec_fn = null;
  var reset_fn = null;
  
  Action ([Integer shared_count]) {
    if (shared_count != null) {
      count = shared_count;
    } else {
      count = new Integer();
    }
    
    // Keep a list of all instantiated actions
    act_list.add(this);
  }
  
  static void reset () {
    for (Action a in act_list) {
      a.count.val = 0;
      if (a.reset_fn != null) {
        a.reset_fn();
      }
    }
  }
  
  bool ready () {
    return count.val < max_count;
  }
  
  bool exec () {
    if (!ready()) {
      return false;
    }
    count.val++;
    exec_fn();
    return true;
  }
}

class State {
  Set peers = new Set();
  bool active = false;
  var init_fn = null;
  Action activate;
  
  State ([State peer]) {
    if (peer != null) {
      peers.addAll(peer.peers);
      peers.add(peer);
      for (State s in peers) {
        s.peers.add(this);
      }
      activate = new Action(peer.activate.count);
    } else {
      activate = new Action();
    }
    
    activate.reset_fn = update;
  }
  
  void update () {
    if (active) {
      init_fn();
      activate.exec_fn();
    }
  }
  
  bool toggle () {
    if (!activate.ready()) {
      return false;
    }
    init_fn();
    if (!active) {
      for (State s in peers) {
        s.active = false;
      }
      activate.exec();
    }
    active = !active;
    return true;
  }
}


var cities;
var colony;



class City {
  var population;
  var dead;
  var infected;
  var getting_sick;
  var healthy;
  var name;
  
  var spread_from_factor;    // Factor applied when spreading from this city
  var spread_to_factor;      // Factor applied when other cities spread to this one
  var spread_within_factor;  // Factor applied when spreading within this city
  var death_rate;            // Rate of infected people dying
  var workforce;             // Number of people producing resources
  
  var production;
  var next_to;
  var x;
  var y;

  var uprooted;              // No production next turn
  
  // Oribital Strike Counter is shared across all cities
  static Integer os_cntr = new Integer();

  Action orbital_strike;
  Action vaccinate;
  Action cull;
  Action uproot;
  
  State barricade;
  State quarantine;
  State house_arrest;
  
  City ()
  {
    orbital_strike = new Action(os_cntr);
    orbital_strike.exec_fn = (){kill(infected, healthy);};
    
    vaccinate = new Action();
    vaccinate.exec_fn = (){
      spread_to_factor *= 0.5;
      spread_from_factor *= 0.5;
      spread_within_factor *= 0.5;
    };
    
    cull = new Action();
    cull.exec_fn = (){
      kill((infected * 0.85).floor(), (healthy * 0.05).ceil());
      workforce *= 0.8;
    };
    
    uproot = new Action();
    uproot.exec_fn = (){
      workforce = (workforce * 1.5).toInt();
      uprooted = true;
    };
        
    barricade = new State();
    barricade.init_fn = configure;
    barricade.activate.exec_fn = (){
      spread_to_factor *= 0.15;
      workforce *= 0.5;
    };
    
    
    quarantine = new State(barricade);
    quarantine.init_fn = configure;
    quarantine.activate.exec_fn = (){
      spread_to_factor *= 0.01;
      spread_from_factor *= 0.01;
      workforce = 0;
    };
    
    house_arrest = new State(quarantine);
    house_arrest.init_fn = configure;
    house_arrest.activate.exec_fn = (){
      spread_within_factor *= 0.5;
      spread_from_factor *= 0.5;
      workforce -= infected;
    };
  }
  
  void configure ()
  {
    spread_to_factor = 1.0;
    spread_from_factor = 1.0;
    spread_within_factor = 1.0;
    death_rate = 1.0;
    workforce = population - dead;
  }

  void kill (var infected_cnt, var healthy_cnt)
  {
    infected_cnt = infected_cnt.toInt();
    healthy_cnt = healthy_cnt.toInt();
    
    dead += infected_cnt;
    infected -= infected_cnt;
    dead += healthy_cnt;
    healthy -= healthy_cnt;
  }
  
  void infect (var spread_factor, var adjust)
  {
    var infection = spread_factor * adjust;
    if (infection > healthy) {
      infection = healthy;
    }
    infection = infection.toInt();
    getting_sick += infection;
    healthy -= infection;
  }
  
  void spread_within ()
  {
    infect(spread_within_const, infected * spread_within_factor);
  }

  void spread_from ()
  {
    var from_rate = infected * spread_from_factor * spread_from_const;
    for (var to_city in next_to) {
      to_city.infect(from_rate, to_city.spread_to_factor);
    }
  }
  
  void succumb ()
  {
    var deaths = infected * death_const * death_rate;
    if (deaths > infected) {
      deaths = infected;
    }
    kill(deaths.ceil(), 0);
  }
  
  void metastacize ()
  {
    infected += getting_sick;
    getting_sick = 0;
  }
  
  int harvest ()
  {
    return (workforce * production).toInt();
  }
}


class Colony
{
  var resources;
  var remaining_turns;
  var remaining;
  var selected_city;
  var os_this_turn;
  
  Colony()
  {
    remaining_turns = game_turns;
    resources = 0;
    remaining = 0;
    os_this_turn = 0;
    
    for (var city in cities) {
      city.metastacize();
      remaining += city.population;
    }
  }
  
  void turn_end ()
  {
    for (var city in cities) {
      resources += city.harvest();
    }
    for (var city in cities) {
      city.spread_within();
    }
    for (var city in cities) {
      city.succumb();
    }
    for (var city in cities) {
      city.spread_from();
    }
    
    remaining = 0;
    for (var city in cities) {
      city.metastacize();
      if (city.uprooted) {
        city.uprooted = false;
      }
      remaining += city.population - city.dead;
    }
    remaining_turns--;
    os_this_turn = 0;
    Action.reset();
  }
  
  int deathToll()
  {
    var toll = 0;
    for (var city in cities) {
      toll += city.dead;
    }
    return toll;
  }
  
  void select(City thisCity)
  {
    selected_city = thisCity;
  }
  
  bool orbital_strike()
  {
    return selected_city.orbital_strike.exec();
  }
  
  bool cull()
  {
    return selected_city.cull.exec();
  }
  
  bool uproot()
  {
    return selected_city.uproot.exec();
  }
  
  bool barricade()
  {
    return selected_city.barricade.toggle();
  }
  
  bool quarantine()
  {
    return selected_city.quarantine.toggle();
  }
  
  bool house_arrest()
  {
    return selected_city.house_arrest.toggle();
  }
  
  bool vaccinate()
  {
    var required = selected_city.healthy;
        
    if (resources >= required && selected_city.vaccinate.exec()) {
      resources -= required;
      return true;
    }
    
    return false;
  }
}


void city_init ()
{
  var city;
  var connx = [[0,1],[0,4],[0,8],
               [1,2],[1,4],[1,5],
               [2,3],[2,5],[2,6],[2,10],
               [3,6],[3,7],
               [4,5],[4,8],[4,9],
               [5,9],[5,10],
               [6,7],[6,10],[6,13],[6,14],
               [7,14],
               [8,9],[8,11],[8,12],
               [9,10],[9,12],
               [10,12],[10,13],
               [11,12],
               [12,13],
               [13,14]];
    
  cities = new List(15);
  for (var c = 0; c < 15; c++) {
    cities[c] = new City();
  }

  city = cities[0];
  city.name = "Constantinople";
  city.population = 15000;
  city.production = 1.25;
  city.getting_sick = 0.04;
  city.x = 58;
  city.y = 126;

  city = cities[1];
  city.name = "Rome";
  city.population = 20000;
  city.production = 1.75;
  city.getting_sick = 0.04;
  city.x = 274;
  city.y = 43;

  city = cities[2];
  city.name = "Barcelona";
  city.population = 55000;
  city.production = 1.75;
  city.getting_sick = 0;
  city.x = 492;
  city.y = 60;

  city = cities[3];
  city.name = "Marsailles";
  city.population = 25000;
  city.production = 1.25;
  city.getting_sick = 0.05;
  city.x = 881;
  city.y = 134;

  city = cities[4];
  city.name = "Milan";
  city.population = 30000;
  city.production = 2.0;
  city.getting_sick = 0.03;
  city.x = 182;
  city.y = 218;

  city = cities[5];
  city.name = "Vienna";
  city.population = 40000;
  city.production = 2.25;
  city.getting_sick = 0;

  city.x = 379;
  city.y = 155;

  city = cities[6];
  city.name = "Paris";
  city.population = 40000;
  city.production = 2.0;
  city.getting_sick = 0;

  city.x = 739;
  city.y = 290;
  
  city = cities[7];
  city.name = "Cologne";
  city.population = 10000;
  city.production = 1.25;
  city.getting_sick = 0.04;
  city.x = 878;
  city.y = 240;

  city = cities[8];
  city.name = "London";
  city.population = 35000;
  city.production = 1.75;
  city.getting_sick = 0;
  city.x = 138;
  city.y = 318;

  city = cities[9];
  city.name = "Copenhagen";
  city.population = 45000;
  city.production = 2.25;
  city.getting_sick = 0;
  city.x = 335;
  city.y = 380;

  city = cities[10];
  city.name = "Stockholm";
  city.population = 500000;
  city.production = 0;
  city.getting_sick = 0;
  city.x = 464;
  city.y = 351;

  city = cities[11];
  city.name = "Moscow";
  city.population = 20000;
  city.production = 1.25;
  city.getting_sick = 0.03;
  city.x = 61;
  city.y = 490;

  city = cities[12];
  city.name = "Kiev";
  city.population = 60000;
  city.production = 2.75;
  city.getting_sick = 0;
  city.x = 361;
  city.y = 512;

  city = cities[13];
  city.name = "Cracow";
  city.population = 50000;
  city.production = 1.75;
  city.getting_sick = 0;
  city.x = 666;
  city.y = 546;

  city = cities[14];
  city.name = "Naples";
  city.population = 35000;
  city.production = 1.5;
  city.getting_sick = 0.04;
  city.x = 857;
  city.y = 439;

  for (city in cities) {
    city.dead = 0;
    city.getting_sick = (city.getting_sick * city.population).toInt();
    city.infected = 0;
    city.healthy = city.population - city.getting_sick;
    city.next_to = new Set();
    city.uprooted = false;
    city.configure();
    
  }
  
  for (var p in connx) {
    cities[p[0]].next_to.add(cities[p[1]]);
    cities[p[1]].next_to.add(cities[p[0]]);
  }
}

