part of darkagesgame;


void clickOnBarricade(MouseEvent e)
{
  if (colony.barricade()) {
    playAudio("culling");
  }
}

void clickOnOrbitalStrike(MouseEvent e)
{
  if(colony.orbital_strike()) {
    playAudio("orbitalStrike");
  }
}

void clickOnQuarantine(MouseEvent e)
{
  if (colony.quarantine()) {
    playAudio("quarantine");
  }
}

void clickOnCull(MouseEvent e)
{
  if (colony.cull()) {
    playAudio("laserBlast"); 
  }
}

void clickOnVaccinate(MouseEvent e)
{
  if (colony.vaccinate()) {
    playAudio("generalUse");
  }
}
void clickOnUproot(MouseEvent e)
{
  if (colony.uproot()) {
    playAudio("uprootSound");
  }
}

void clickOnHouseArrest(MouseEvent e)
{
  if (colony.house_arrest()) {
    playAudio("houseArrest");
  }
}

void stepTurn(MouseEvent e){
  playAudio("endTurn");
  print("stepTurn");
  colony.turn_end();
  for(var menu in listOfMenus){
    menu.updateStatusBar();
  }
  
  spaceShip.x += 32; //move the spaceship
  
  if (colony.remaining_turns <= 0 )
  {
    endGame();
    playAudio("victory");
  }
  if (colony.remaining == 0)
  {
    endGame();
    playAudio("death");
  }
  
  if (colony.remaining_turns == 5)
  {
    fadeTensionIn();
  }
  
}